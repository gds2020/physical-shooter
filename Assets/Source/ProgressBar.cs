﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Color color;
    [SerializeField] Transform pivotXF;
    [SerializeField] Renderer materialDestination;


    Transform cameraXF;



    public float Progress
    {
        set
        {
            float f = Mathf.Clamp01(value);
             pivotXF.localScale = new Vector3(f, 1f, 1f);
        }
    }

    public bool Visible
    {
        get
        {
            return gameObject.activeSelf;
        }

        set
        {
            gameObject.SetActive(value);
        }
    }


    void Awake()
    {
        materialDestination.material.color = color;  // clone/replace
        cameraXF = Camera.main.transform;
    }


    void LateUpdate()
    {
        transform.rotation = cameraXF.rotation;
    }

}
