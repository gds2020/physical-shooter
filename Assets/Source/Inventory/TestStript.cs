﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using InventorySystem;


public class TestStript
{

    [MenuItem("Tools/Generate Random Inventory")]
    static void TestInventory()
    {
        throw new System.NotImplementedException();
        /*
        var inv = new Inventory();

        for (int i = 0; i < 10; i++)
            inv.AddItem(new Item( FindItemAsset() ));


        GameObject canvas = GameObject.FindGameObjectWithTag("canvas-ui");
        Assert.IsNotNull(canvas);

        var invCtrl = canvas.GetComponentInChildren<UIInventoryController>(true);
        Assert.IsNotNull(invCtrl);

        invCtrl.ShowAndSetup(inv);
        */
    }


    static ItemAsset FindItemAsset()
    {
        string filter = $"t:{nameof(ItemAsset)}";
        string assetGuid = GetRandomGuid( AssetDatabase.FindAssets(filter) );
        string assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);
        ItemAsset asset = AssetDatabase.LoadAssetAtPath<ItemAsset>(assetPath);
        Assert.IsNotNull(asset, "asset not found");
        return asset;
    }

    static string GetRandomGuid(string[] guids)
    {
        if (guids.Length == 0) throw new System.Exception("no asset found");
        int randomIndex = Random.Range(0, guids.Length);
        return guids[randomIndex];
    }
}
