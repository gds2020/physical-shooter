﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;



namespace InventorySystem
{
    public class Inventory
    {
        private List<Item> allItems = new List<Item>();


        //............PROP

        public Item[] AllItems => allItems.ToArray();


        public void AddItem(Item i)
        {
            Assert.IsNotNull(i);
            Assert.IsFalse(allItems.Contains(i), "double add item");
            allItems.Add(i);
        }
    }
}