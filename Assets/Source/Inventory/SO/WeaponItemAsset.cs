﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




namespace InventorySystem
{
    [CreateAssetMenu(menuName = "InventorySystem/Weapon Item", fileName = "WeaponItem")]
    public class WeaponItemAsset : ItemAsset
    {
        public UIShellClip uiShellClipPrefab;
        public AudioClip soundFire;
        public AudioClip soundReload;
        public float reloadTime;
        public float fireCooldown;
        public int bulletCapacity;


        public override EItemType ItemType => EItemType.Weapon;


        protected override void OnValidate()
        {
            base.OnValidate();

            Assert.IsNotNull(uiShellClipPrefab);
            Assert.IsNotNull(soundFire);
            Assert.IsNotNull(soundReload);

            Assert.IsTrue(reloadTime > 0f);
            Assert.IsTrue(fireCooldown > 0f);
            Assert.IsTrue(bulletCapacity > 0);
        }
    }
}