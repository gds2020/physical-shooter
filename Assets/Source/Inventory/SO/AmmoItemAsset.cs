﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace InventorySystem
{
    [CreateAssetMenu(menuName = "InventorySystem/Ammo Item", fileName = "AmmoItem")]
    public class AmmoItemAsset : ItemAsset
    {
        public override EItemType ItemType => EItemType.Ammo;
    }
}