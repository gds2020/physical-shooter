﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace InventorySystem
{
    [CreateAssetMenu(menuName = "InventorySystem/Consumable Item", fileName = "ConsumableItem")]
    public class ConsumableItemAsset : ItemAsset
    {
        public override EItemType ItemType => EItemType.Consumable;
    }
}