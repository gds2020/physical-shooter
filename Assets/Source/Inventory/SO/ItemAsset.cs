﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace InventorySystem
{

    public enum EItemType { Weapon, Ammo, Consumable }





    //[CreateAssetMenu(menuName = "InventorySystem/BasicItem", fileName = "BasicItem")]
    public abstract class ItemAsset : ScriptableObject
    {
        public GameObject prefab;
        public Sprite icon;
        public float mass = 1f;
        public string itemName;
        [TextArea] public string description;

        public abstract EItemType ItemType {get;}



        protected virtual void OnValidate()
        {
            if (prefab == null)
                Debug.LogError("Prefab not set", this);
        }
    }
}