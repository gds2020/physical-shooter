﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;



namespace InventorySystem
{
    public abstract class Item
    {
        protected ItemAsset baseAsset;


        public string Name        => baseAsset.itemName;
        public float  Mass        => baseAsset.mass;
        public string Description => baseAsset.description;
        public EItemType Type     => baseAsset.ItemType;
        public Sprite Icon        => baseAsset.icon;
        public GameObject Prefab  => baseAsset.prefab;


        public Item(ItemAsset asset)
        {
            Assert.IsNotNull(asset);
            baseAsset = asset;
        }
    }
}