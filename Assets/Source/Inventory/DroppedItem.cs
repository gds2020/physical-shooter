﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using InventorySystem;

public class DroppedItem : MonoBehaviour
{

    [SerializeField] Transform slotXf;
    [SerializeField] float rotateSpeed = 1f;
    [SerializeField] ItemAsset asset;

    Item item_; // baking field
    public Item Item {
        get {
            Assert.IsNotNull(item_);
            return item_;
        }
        set {
            Assert.IsNotNull(value);
            item_ = value;
            this.CreateObject();
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        var player = other.GetComponentAsserted<PlayerController>();
        player.AddItem(Item);
        Destroy(this.gameObject);
    }


    private void CreateObject()
    {
        Assert.IsNotNull(Item.Prefab);
        var sceneObject = GameObject.Instantiate(Item.Prefab);
        sceneObject.transform.SetParent(slotXf, false);
        sceneObject.transform.ResetTransform();
    }



    void Start()
    {
        if (asset != null)
            ItemSpawner.Instance.SpawnItem(this, asset);
    }

    void Update()
    {
        slotXf.Rotate(0f, rotateSpeed * Time.deltaTime, 0f, Space.Self);
    }

}
