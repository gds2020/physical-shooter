﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;



namespace InventorySystem
{
    public class ItemSpawner : MonoBehaviour
    {
        public static ItemSpawner Instance;


        [SerializeField] DroppedItem droppedItemPrefab;
        ItemAsset[] allItems;


        void Awake()
        {
            if (Instance != null) throw new System.InvalidProgramException();
            Instance = this;

            allItems = Resources.LoadAll<ItemAsset>("Items");
            Assert.IsFalse(allItems.Length == 0);
            Debug.LogFormat("Found {0} assets", allItems.Length);

            FindEnemySpawner().OnSomeEnemyKill += SomeEnemyKilledHandler;
        }


        public void SpawnItem(DroppedItem slot, ItemAsset asset)
        {
            Assert.IsNotNull(slot);
            Assert.IsNotNull(asset);


            switch (asset.ItemType)
            {
                case EItemType.Ammo:
                    slot.Item = new AmmoItem(asset);
                    break;
                case EItemType.Weapon:
                    slot.Item = new WeaponItem((WeaponItemAsset)asset);
                    break;
                case EItemType.Consumable:
                    slot.Item = new ConsumableItem(asset);
                    break;

                default: throw new System.NotImplementedException($"item tepe: {asset.ItemType}");
            }
        }


        private void SpawnItem(ItemAsset asset, Vector3 position)
        {
            var slot = GameObject.Instantiate<DroppedItem>(droppedItemPrefab);
            slot.transform.ResetTransform();
            slot.transform.position = position;
            this.SpawnItem(slot, asset);
        }


        private void SomeEnemyKilledHandler(Vector3 position)
        {
            ItemAsset randomAsset = this.GetRandomAsset();
            this.SpawnItem(randomAsset, position);
        }


        private ItemAsset GetRandomAsset() => allItems.RandomElement();

        private Spawner FindEnemySpawner() => this.GetComponentAsserted<Spawner>();

    }
}