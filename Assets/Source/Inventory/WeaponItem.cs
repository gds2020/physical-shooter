﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace InventorySystem
{
    public class WeaponItem : Item
    {

        public int bulletCount;
        public WeaponItemAsset WeaponAsset => (WeaponItemAsset)baseAsset;

        //..................................................................................PROPERTY

        private WeaponItemAsset Asset => (WeaponItemAsset)this.baseAsset;

        public UIShellClip UIShellClipPrefab => Asset.uiShellClipPrefab;


        public WeaponItem(WeaponItemAsset asset) : base(asset)
        {
            bulletCount = asset.bulletCapacity;
        }
    }
}