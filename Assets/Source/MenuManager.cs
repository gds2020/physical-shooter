﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;




public class MenuManager : MonoBehaviour
{
    private const string KEY_MUSIC = "music-volume";
    private const string KEY_SFX   = "sfx-volume";


    [Header("Root Objects")]
    [SerializeField] GameObject _mainMenuRoot;
    [SerializeField] GameObject _optionsRoot;


    [SerializeField] Button _buttonPlay,
                            _buttonOptions,
                            _buttonExit;

    [Header("Options")]
    [SerializeField] Button _buttonOptionExit;
    [SerializeField] Toggle _toogleOptionBlood;
    [SerializeField] Slider _sliderOptionMusic,
                            _sliderOptionEffects;


    void Awake()
    {
        _buttonPlay   .onClick.AddListener(OnPlayClicked);
        _buttonOptions.onClick.AddListener(OnOptionsClicked);
        _buttonExit   .onClick.AddListener(OnExitClicked);

        _buttonOptionExit.onClick.AddListener(OnOptionExit);

        _sliderOptionMusic  .onValueChanged.AddListener( x => this.SetAudioVolume(KEY_MUSIC, x) );
        _sliderOptionEffects.onValueChanged.AddListener( x => this.SetAudioVolume(KEY_SFX, x) );
        _toogleOptionBlood.onValueChanged  .AddListener( b => SerializeManager.Instance.Set("blood", b));
    }


    void Start()
    {
        // NOTE: 47,49 this will write to PlayerPrefs back when deserialized
        // music
        _sliderOptionMusic.value = float.Parse(SerializeManager.Instance.Get(KEY_MUSIC, "0.7"));
        // sfx
        _sliderOptionEffects.value = float.Parse(SerializeManager.Instance.Get(KEY_SFX, "0.7"));
        // blood
        bool isBloodOn   = bool.Parse(SerializeManager.Instance.Get("blood", "False"));
        _toogleOptionBlood.SetIsOnWithoutNotify(isBloodOn);
        // play music in menu
        AudioManager.Instance.PlayMusic(AudioManager.Instance.SoundDB.menuClip);
    }


    private void OnOptionExit()
    {
        _optionsRoot.SetActive(false);
        _mainMenuRoot.SetActive(true);
        SerializeManager.Instance.Save();
    }

    void OnEnable()
    {
        _mainMenuRoot.SetActive(true);
        _optionsRoot.SetActive(false);
    }


    private void OnPlayClicked()
    {
        SceneManager.LoadScene(Const.SceneNames.Game);
    }


    private void OnOptionsClicked()
    {
        _optionsRoot.SetActive(true);
        _mainMenuRoot.SetActive(false);
    }


    private void OnExitClicked()
    {
        Application.Quit();
    }


    private void SetAudioVolume(string paramName, float volume)
    {
        SerializeManager.Instance.Set(paramName, volume);
        AudioManager.Instance.SetMixerParam(paramName, volume);
    }
}
