﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalController : MonoBehaviour
{
    [SerializeField] PortalNode a, b;
    [SerializeField] Color color;



    void Awake()
    {
        a.SetColor(color);
        b.SetColor(color);

        a.IsActive = true;
        b.IsActive = true;
    }



    public void TeleportObject(Rigidbody who, PortalNode source)
    {
        PortalNode destination = source == a ? b : a;
        destination.IsActive = false;
        StartCoroutine(TeleportObjectCR(who, destination));
    }


    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.2f, 0.4f, 0.9f);
        Gizmos.DrawLine(a.transform.position, b.transform.position);
    }


    IEnumerator TeleportObjectCR(Rigidbody rb, PortalNode destination)
    {
        rb.gameObject.SetActive(false);
        a.IsActive = b.IsActive = false;
        yield return new WaitForSeconds(1f);
        rb.gameObject.SetActive(true);
        rb.position = destination.transform.position;
        PortalNode source = destination == a ? b : a;
        source.IsActive = true;
    }
}
