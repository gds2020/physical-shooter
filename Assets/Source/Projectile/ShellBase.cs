﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public abstract class ShellBase : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float autoDestroyTime = 1f;
    [SerializeField] float damage = 100f;
    [SerializeField] ParticleSystem groundSplashParticles;
    [SerializeField] ParticleSystem wallSplashParticles;
    [SerializeField] ParticleSystem bloodSplashParticles;

    Rigidbody rbody;
    Vector3 prevVelocity;


    void Awake()
    {
        Vector3 dir = this.transform.forward;
        Vector3 worldVelocity = dir * speed;

        rbody = GetComponent<Rigidbody>();
        rbody.velocity = worldVelocity;
        // NOTE: Autodestroy
        Destroy(gameObject, autoDestroyTime);
    }


    void FixedUpdate()
    {
        prevVelocity = rbody.velocity;
    }


    void OnCollisionEnter(Collision collision)
    {
        var damageable = collision.collider.GetComponent<IDamageable>();
        if (damageable != null)
        {
            float currentSpeed = prevVelocity.magnitude;
            float currentDamage = GetDamege(currentSpeed);
            damageable.TakeDamage(currentDamage, prevVelocity.normalized);
            ShowParticleFX(collision, bloodSplashParticles);
            ShellDirectImpactShutdown();
        }
        else // hard body
        {
            float currSpeed = rbody.velocity.magnitude;
            float speedRatio = currSpeed / speed;
            if (speedRatio < 0.6f)
            {
                bool didHitGround = collision.collider.gameObject.layer == LayerMask.NameToLayer("Ground");
                ShowParticleFX(collision, didHitGround ? groundSplashParticles : wallSplashParticles);
                ShellDirectImpactShutdown();
            }
        }

    }


    private void ShellDirectImpactShutdown()
    {
        this.GetComponent<Collider>().enabled = false;
        this.enabled = false;
        // disable all children
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
            transform.GetChild(i).gameObject.SetActive(false);
        // fix rigidbody updates
        rbody.velocity = Vector3.zero;
        rbody.useGravity = false;
        rbody.Sleep();
    }


    private void ShowParticleFX(Collision collision, ParticleSystem particleSystem)
    {
        Vector3 position = collision.contacts[0].point;
        Vector3 normal   = collision.contacts[0].normal;
        ParticleSystem ps = Instantiate(particleSystem, position, Quaternion.identity);
        Destroy(ps.gameObject, 10f);                            // HACK: think how to do this better
        ps.transform.forward = normal;
        ps.Play();
    }

    float GetDamege(float currentSpeed)
    {
        float speedFactor = currentSpeed / speed;
        return damage * Mathf.Pow(speedFactor, 2);
    }

}
