﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class SerializeManager
{
    public static SerializeManager Instance {get;}
    private const string KEY_MUSIC = "music";


    static SerializeManager()
    {
        Instance = new SerializeManager();
    }

    private SerializeManager() { }




    public void Set(string paramName, object data)
    {
        string paramValue = data.ToString();
        PlayerPrefs.SetString(paramName, paramValue);
    }

    public string Get(string paramName, string defaultValue)
    {
        return PlayerPrefs.GetString(paramName, defaultValue);
    }

    public void Save()
    {
        PlayerPrefs.Save();
    }

}
