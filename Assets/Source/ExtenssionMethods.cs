﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




public static class ExtenssionMethods
{
    public static void ResetTransform(this Transform xf)
    {
        if (xf == null) return;
        xf.localPosition = Vector3.zero;
        xf.localRotation = Quaternion.identity;
        xf.localScale    = Vector3.one;
    }


    public static T GetComponentAsserted<T>(this Component cmp) where T : Component
    {
        Assert.IsNotNull(cmp);
        T result = cmp.GetComponent<T>();
        Debug.Assert(result != null, $"could not found component of type '{typeof(T).Name}' on object {cmp.name}", cmp);
        return result;
    }


    public static T GetComponentAsserted<T>(this GameObject go) where T : Component
    {
        Assert.IsNotNull(go);
        T result = go.GetComponent<T>();
        Debug.Assert(result != null, $"could not found component of type '{typeof(T).Name}' on object {go.name}", go);
        return result;
    }


    /// <summary> Get a random element from any array, and return it </summary>
    public static T RandomElement<T>(this T[] array)
    {
        Assert.IsNotNull(array);
        if (array.Length == 0) return default(T);
        return array[Random.Range(0, array.Length)];
    }
}
