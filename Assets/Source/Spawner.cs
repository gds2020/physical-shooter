﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    [SerializeField] GameObject enemyPrototype;
    [SerializeField] StartingPoints startingPoints;
    [SerializeField] SpawnerEnemyWave[] waves;

    int enemyCount;


    public event Action<Vector3> OnSomeEnemyKill;


    IEnumerator Start()
    {
        PlayerController playerController = GetPlayerDamagebleInterface();
        playerController.OnDead += this.PlayerDeathHandler;

        for (int waveIdx = 0; waveIdx < waves.Length; waveIdx++)
        {

            while (enemyCount > 0)
                yield return null;

            yield return new WaitForSeconds(waves[waveIdx].afterWaveDelay);

            for (int enemyIdx = 0; enemyIdx < waves[waveIdx].count; enemyIdx++)
            {
                Vector3 pos = startingPoints.GetRandomStartPosition();
                var go = Instantiate(enemyPrototype, pos, Quaternion.identity, transform);
                var enemyController = go.GetComponent<EnemyController>();
                Debug.Assert(enemyController != null);
                enemyController.OnDead += this.EnemyDeadHandler;
                enemyCount++;
                yield return new WaitForSeconds(waves[waveIdx].delay);
            }
        }
    }


    private void EnemyDeadHandler(Vector3 deathPosition)
    {
        Debug.Assert(enemyCount > 0);
        enemyCount--;
        OnSomeEnemyKill?.Invoke(deathPosition);
    }


    private void PlayerDeathHandler(Vector3 deathPostion)
    {
        this.StopAllCoroutines();
        foreach (var enemy in this.GetComponentsInChildren<EnemyController>())
            enemy.StopEnemy();
    }


    private PlayerController GetPlayerDamagebleInterface()
    {
        GameObject playerGO = GameObject.FindGameObjectWithTag(PlayerController.TAG);
        Debug.Assert(playerGO != null, "can't find player object");
        var c = playerGO.GetComponent<PlayerController>();
        Debug.Assert(c != null);
        return c;
    }
}





[System.Serializable]
public struct SpawnerEnemyWave
{
    public float delay;
    public float afterWaveDelay;
    public int count;
}
