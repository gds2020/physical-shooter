﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(MeshRenderer))]
public class EnemyController : MonoBehaviour, IDamageable
{

    const float ATTACK_DISTANCE = 4f;

    [SerializeField] float autodestroyTime = 1f;
    [SerializeField] float pathfindingCountPerSecon = 10f;
    [SerializeField] float health = 100f; // TODO: extract into scriptable object
    [SerializeField] ParticleSystem ps;
    [SerializeField] ProgressBar healthBar;
    [SerializeField] float deliverDamegeAmmount = 10f;


    private NavMeshAgent agent;
    private Transform playerXF;
    private float nextPathfindingTime;
    private float currHealth;
    private float defaultAgentSpeed;



    private Rigidbody RBody {
        get {
            if (rbody_ == null) {
                rbody_ = this.GetComponent<Rigidbody>();
                Debug.Assert(rbody_ != null, "component not found");
            }
            return rbody_;
        }
    } Rigidbody rbody_;


    public float Health => currHealth;



    public event Action<Vector3> OnDead;


    public void TakeDamage(float damage, Vector3 direction)
    {
        if (currHealth <= 0f) return;

        currHealth -= damage;
        if (currHealth <= 0f)
        {
            currHealth = 0f;
            ps.transform.forward = direction;
            Autodestroy();
        }

        healthBar.Progress = currHealth / health;
    }


    private void Autodestroy()
    {
        this.StopEnemy();
        this.GetComponent<Collider>().enabled = false;
        this.GetComponent<Renderer>().enabled = false;
        ps.Play();
        healthBar.gameObject.SetActive(false);
        OnDead.Invoke(transform.position);
        Destroy(this.gameObject, autodestroyTime);
    }


    public void StopEnemy()
    {
        this.enabled = false;
        this.agent.enabled = false;
    }


    void Awake()
    {
        agent = this.GetComponent<NavMeshAgent>();
        GameObject playerGO = GameObject.FindGameObjectWithTag(PlayerController.TAG);
        Debug.Assert(playerGO != null, "can't find player object");
        playerXF = playerGO.transform;
        currHealth = health;
        healthBar.Progress = 1f;
        defaultAgentSpeed = agent.speed;
    }


    void FixedUpdate()
    {
        float pathfindingDelay = 1f / pathfindingCountPerSecon;
        if (Time.time > nextPathfindingTime)
        {
            agent.SetDestination(playerXF.position);
            nextPathfindingTime = Time.time + pathfindingDelay;
        }

        if ((playerXF.position - transform.position).sqrMagnitude < ATTACK_DISTANCE*ATTACK_DISTANCE)
        {
            agent.speed = defaultAgentSpeed * 100f;
            RBody.AddForce(RBody.velocity.normalized * 20f);
        }
        else
        {
            // test for max speed
            if (RBody.velocity.magnitude > defaultAgentSpeed)
            {
                RBody.velocity = RBody.velocity.normalized * defaultAgentSpeed;
                agent.speed = defaultAgentSpeed;
            }
        }
    }




    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != PlayerController.TAG)
            return;

        var iDamageble = collision.gameObject.GetComponent<IDamageable>();
        Debug.Assert(iDamageble != null);
        Vector3 forceDirection = -collision.contacts[0].normal;
        iDamageble.TakeDamage(deliverDamegeAmmount, forceDirection);
    }

}
