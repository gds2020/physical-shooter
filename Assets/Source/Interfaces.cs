﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public interface IDamageable
{
    float Health { get; }

    event Action<Vector3> OnDead;

    void TakeDamage(float damage, Vector3 direction);                // TODO: remove direction param
}