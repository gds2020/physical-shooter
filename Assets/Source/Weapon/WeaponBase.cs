﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public abstract class WeaponBase : MonoBehaviour
{
    public enum EFireType { Semiautomatic, Automatic }
    static int FireModeCount = -1;


    [SerializeField] ShellBase shell;
    [SerializeField] Transform muzzleXF;
    public EFireType fireType;



    float nextFireTime;
    bool canSwitchFireMode;
    ProgressBar reloadBar;

    InventorySystem.WeaponItem invItem;
    UIShellClipSlot uiShellSlot;


    public Transform MuzzleXF => muzzleXF;
    public bool CanSwitchFireMode => canSwitchFireMode;


    void Awake()
    {
        canSwitchFireMode = (fireType == EFireType.Automatic);
        if (FireModeCount == -1)
            FireModeCount = System.Enum.GetNames(typeof(EFireType)).Length;
    }



    public void Fire()
    {
        if (invItem.bulletCount <= 0) return;
        if (Time.time < nextFireTime) return;

        Instantiate(shell, MuzzleXF.position, MuzzleXF.rotation);
        nextFireTime = Time.time + invItem.WeaponAsset.fireCooldown;
        invItem.bulletCount--;

        AudioManager.Instance.PlaySFX(invItem.WeaponAsset.soundFire, muzzleXF.position);
        UpdateUIElements();
    }


    public void Reload()
    {
        if (!reloadBar.Visible)
        {
            StartCoroutine( ReloadRoutine() );
            AudioManager.Instance.PlaySFX(invItem.WeaponAsset.soundReload, muzzleXF.position);
        }
        
    }


    public void SwitchFireMode()
    {
        Assert.IsTrue(canSwitchFireMode, "can't call this function for weapons that can't swithc mode");
        int current = (int)fireType;
        current++;
        int newMode = current % FireModeCount;
        fireType = (EFireType)newMode;
    }


    //HACK: should be done via events ReloadRoutine
    public void SetReloadBar(ProgressBar bar)
    {
        Debug.Assert(bar != null);
        reloadBar = bar;
    }


    public void SetInventoryItem(InventorySystem.WeaponItem item)
    {
        Assert.IsNotNull(item);
        this.invItem = item;
        UpdateUIElements();
    }



    private void UpdateUIElements()
    {
        if (uiShellSlot == null)
        {
            var go = GameObject.FindGameObjectWithTag("canvas-ui");
            Debug.Assert(go != null, "can't find object in scene");

            Debug.Assert(go.GetComponentsInChildren<UIShellClipSlot>().Length == 1);

            uiShellSlot = go.GetComponentInChildren<UIShellClipSlot>();
            Debug.Assert(uiShellSlot != null, "can't find component in hierarchy");
        }
        uiShellSlot.Active.DisplayedCount = invItem.bulletCount;
    }


    IEnumerator ReloadRoutine()
    {
        Debug.Assert(reloadBar != null, "progress bar should be set first by SetReloadBar()");
        float startTime = Time.time;
        reloadBar.Visible = true;
        invItem.bulletCount = 0;

        while (Time.time < startTime + invItem.WeaponAsset.reloadTime)
        {
            float elapsedTime = Time.time - startTime;
            reloadBar.Progress = elapsedTime / invItem.WeaponAsset.reloadTime;
            yield return null;
        }

        reloadBar.Progress = 1f;
        invItem.bulletCount = invItem.WeaponAsset.bulletCapacity;
        UpdateUIElements();
        yield return new WaitForSeconds(0.3f);
        reloadBar.Visible = false;
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(MuzzleXF.position, MuzzleXF.forward * 10f);

        if (this.transform.parent != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.parent.position, transform.parent.forward * 10f);
        }
    }
}
