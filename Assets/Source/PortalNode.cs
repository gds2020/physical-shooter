﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(Collider))]
public class PortalNode : MonoBehaviour
{


    [SerializeField] float triggerDistance = 0.1f;
    [SerializeField] float sleepTime;
    [SerializeField] GameObject visualElement;
    [SerializeField] GameObject recolorElementsRoot;
    [SerializeField] ParticleSystem fxParticles;


    private Transform triggerXF;

    public bool IsActive {
        get => _isactive_;
        set {
            _isactive_ = value;
            visualElement.SetActive(value);
        }
    } private bool _isactive_;


    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        triggerXF = other.transform;
        if (!IsActive) return;

        float distance = (transform.position - other.transform.position).magnitude;
        if (distance < triggerDistance)
            TeleportObject(other.attachedRigidbody);
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        IsActive = true;
        triggerXF = null;
    }


    public void SetColor(Color color)
    {
        var allRenderers = recolorElementsRoot.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < allRenderers.Length; i++)
            allRenderers[i].material.color = color;

        fxParticles.startColor = color;
    }


    private void TeleportObject(Rigidbody teleportee)
    {
        IsActive = false;
        Debug.Assert(transform.parent != null, "should have parent");
        var parentController = transform.parent.GetComponent<PortalController>();
        Debug.Assert(parentController != null, "expected component not found");
        parentController.TeleportObject(teleportee, this);
        fxParticles.Play();
    }


    void OnDrawGizmos()
    {
        if (triggerXF == null) return;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, triggerXF.position);
    }

}
