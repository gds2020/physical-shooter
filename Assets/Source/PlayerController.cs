﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using InventorySystem;




[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour, IDamageable
{
    public const string TAG = "Player";
    public const float PUSH_FORCE = 20f;  // TODO: extract into SO

    [SerializeField] float force;
    [SerializeField] float maxVelocity = 10f;
    [SerializeField] float minAimingDistance = 1f;
    [SerializeField] PlayerControlMode controlMode;
    [SerializeField] Transform crosshairXF;
    [SerializeField] Transform weaponSlotXF;
    [SerializeField] ProgressBar reloadBar;


    private Inventory inventory;
    private Rigidbody rbody;
    private UIProgressBar uiHealthBar;
    private UIInventoryController uiInventory;
    private UIGameMenu uiGameMenu;
    private WeaponBase weapon;
    private float maxHealth;



    public float Health {get; private set;} = 100f;  // TODO: extract into scriptable object

    public event Action<Vector3> OnDead;


    public void TakeDamage(float damage, Vector3 direction)
    {
        if (Health == 0f) return;

        Assert.IsFalse(damage < 0f);
        Health = Mathf.Max(0f, Health - damage);

        uiHealthBar.Value = Health / maxHealth;
        rbody.AddForce(direction * PUSH_FORCE, ForceMode.Impulse);

        if (Health == 0f)
            PlayerGameOver();

        AudioManager.Instance.SetDamage();
    }

    public void AddItem(Item i)
    {
        Assert.IsNotNull(inventory);
        Assert.IsNotNull(i);
        inventory.AddItem(i);

        if (i.Type == EItemType.Weapon)
            EquipWeapon((WeaponItem)i);

        // if (i is WeaponItem weaponItem) // c# 7
        //     EquipWeapon(weaponItem);
    }


    public void EquipWeapon(WeaponItem i)
    {
        if (weapon != null) Destroy(weapon.gameObject);

        var go = GameObject.FindGameObjectWithTag("canvas-ui");
        Debug.Assert(go != null, "can't find object in scene");
        var uiSlot = go.GetComponentInChildren<UIShellClipSlot>();
        Debug.Assert(uiSlot != null);
        uiSlot.CreateShellClip(i.UIShellClipPrefab);

        GameObject sceneObject = GameObject.Instantiate(i.Prefab);
        WeaponBase newWeapon = sceneObject.GetComponent<WeaponBase>();
        newWeapon.SetInventoryItem(i);
        Assert.IsNotNull(newWeapon, "given item is not a weapon");
        this.EquipWepon(newWeapon);
        // create ui element
    }


    public void UseConsumable(Item i)
    {
        throw new System.NotImplementedException();
    }


    void Awake()
    {
        rbody = this.GetComponent<Rigidbody>();
        uiHealthBar = this.GetUIController<UIProgressBar>();
        uiInventory = this.GetUIController<UIInventoryController>();
        uiGameMenu  = this.GetUIController<UIGameMenu>();

        reloadBar.Visible = false;
        maxHealth = Health;
        inventory = new Inventory();
    }


    void Start()
    {
        uiHealthBar.Value = 1f;
        AudioManager.Instance.PlayMusic(AudioManager.Instance.SoundDB.gameClip);
    }


    void Update()
    {
        if (Health == 0f) return;
        // Left Mouse (fire)
        if (weapon != null && !uiInventory.IsVisible)
        {
            // R key (reload)
            if (Input.GetKeyDown(KeyCode.R))
            {
                weapon.SetReloadBar(reloadBar);
                weapon.Reload();
            }
            if (weapon.CanSwitchFireMode && Input.GetKeyDown(KeyCode.B))
            {
                weapon.SwitchFireMode();
            }
            if (weapon.fireType == WeaponBase.EFireType.Automatic)
            {
                if (Input.GetMouseButton(0))
                    weapon.Fire();
            }
            else if (weapon.fireType == WeaponBase.EFireType.Semiautomatic)
            {
                if (Input.GetMouseButtonDown(0))
                    weapon.Fire();
            }
            else
                throw new System.NotImplementedException("unknow fire type");
        }

        // I key (inventory)
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (uiInventory.IsVisible == false)
                this.ShowInventory(true);
            else
                this.ShowInventory(false);
        }
        // TAB Key (inventory)
        if (Input.GetKeyDown(KeyCode.Tab) && uiInventory.IsVisible == false)
            this.ShowInventory(true);
        if (Input.GetKeyUp(KeyCode.Tab) && uiInventory.IsVisible)
            this.ShowInventory(false);

        // Game Menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.ShowGameMenu(!uiGameMenu.IsVisible);
        }

    }

    void FixedUpdate()
    {
        LookAtMousePosition();

        if (rbody.velocity.sqrMagnitude < maxVelocity*maxVelocity)
        {
            float verticalAxis   = Input.GetAxis("Vertical");
            float horizontalAxis = Input.GetAxis("Horizontal");

            if (controlMode == PlayerControlMode.World)
                rbody.AddForce(force * horizontalAxis, 0f, force * verticalAxis);
            else if (controlMode == PlayerControlMode.Local)
                rbody.AddRelativeForce(force * horizontalAxis, 0f, force * verticalAxis);
            else throw new System.NotImplementedException(controlMode.ToString());
        }
    }

    void OnDestroy()
    {
        Time.timeScale = 1f;
    }


    private void EquipWepon(WeaponBase newWeapon)
    {
        Assert.IsNotNull(newWeapon);
        this.weapon = newWeapon;

        newWeapon.transform.SetParent(weaponSlotXF, false);
        newWeapon.transform.ResetTransform();
    }


    private void LookAtMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        int raycastMask = LayerMask.GetMask("Ground", "InfiniteCollider");
        bool wasHit = Physics.Raycast(ray, out hitInfo, 1000f, raycastMask);
        if (!wasHit) return;

        bool wasGroundHit = hitInfo.collider.gameObject.tag != "inifinite-collider";
        Cursor.visible = !wasGroundHit;
        crosshairXF.gameObject.SetActive(wasGroundHit);


        Vector3 hitPosition = hitInfo.point;
        Vector3 lookPosition = hitPosition;
        // rotate player to target
        lookPosition.y = this.transform.position.y;
        this.transform.LookAt(lookPosition);
        // rotate weapon slot to target
        Vector3 playerGroundPosition = this.transform.position;
        playerGroundPosition.y = hitPosition.y;
        if ((playerGroundPosition - hitPosition).magnitude > minAimingDistance)
        {
            lookPosition.y = weaponSlotXF.position.y;
            this.weaponSlotXF.LookAt(lookPosition);
        }
        // crosshair move
        //NOTE: refactor this into class
        crosshairXF.position = hitPosition;
    }


    private void PlayerGameOver()
    {
        this.enabled = false;
        Cursor.visible = true;
        crosshairXF.gameObject.SetActive(false);
        OnDead.Invoke(transform.position);
    }


    private T GetUIController<T>() where T : MonoBehaviour
    {
        var canvas = GameObject.FindGameObjectWithTag("canvas-ui");            // this is the canvas obj
        Debug.Assert(canvas != null, "can't find canvas object");
        T cmp = canvas.GetComponentInChildren<T>(true);
        Debug.Assert(cmp != null, $"can't find UI controller of type '{typeof(T).Name}'");
        return cmp;
    }


    private void ShowInventory(bool willShow)
    {
        Debug.Assert(willShow == !uiInventory.IsVisible);
        if (willShow)
            uiInventory.ShowAndSetup(inventory);
        else
            uiInventory.Hide();

        Cursor.visible = willShow;
        Time.timeScale = willShow ? 0f : 1f;
    }


    private void ShowGameMenu(bool willShow)
    {
        uiGameMenu.IsVisible = willShow;
        Cursor.visible = willShow;
        Time.timeScale = willShow ? 0f : 1f;
    }

}



public enum PlayerControlMode
{
    Local,
    World,
}