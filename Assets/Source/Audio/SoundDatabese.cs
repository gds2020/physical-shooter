﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[CreateAssetMenu(menuName = "AudioDatabase")]
public class SoundDatabese : ScriptableObject
{
    public AudioClip menuClip;
    public AudioClip gameClip;
}
