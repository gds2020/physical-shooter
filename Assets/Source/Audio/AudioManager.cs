﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Audio;




public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;


    [SerializeField] SoundDatabese soundDatabese;
    [SerializeField] Transform rootSfx;
    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioMixerGroup sfxMixerGroup;


    List<AudioSource> sfxList;
    IEnumerator damageCoroutine;


    public SoundDatabese SoundDB => soundDatabese;

    void Awake()
    {
        if (Instance != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }

        Instance = this;
        sfxList = new List<AudioSource>();
        GameObject.DontDestroyOnLoad(this);
    }


    // PUBLIC

    public void PlaySFX(AudioClip soundClip, Vector3 soundPosition = default)
    {
        Assert.IsNotNull(soundClip);
        AudioSource asource = GetFreeSfxSource();
        asource.clip = soundClip;
        asource.Play();
        asource.transform.position = soundPosition;
    }


    public void PlayMusic(AudioClip musicClip)
    {
        Assert.IsNotNull(musicClip);
        musicSource.clip = musicClip;
        musicSource.Play();
    }


    public void SetMixerParam(string paramName, float value)
    {
        AudioMixer mixer = sfxMixerGroup.audioMixer;

        value = Mathf.Clamp(value, 0.001f, 1f);
        float attenuation = Mathf.Log(value) * 20f;
        mixer.SetFloat(paramName, attenuation);
    }

    public void SetDamage()
    {
        if (damageCoroutine != null) return;
        damageCoroutine = SnapshotTransitionCR(1f);
        StartCoroutine(damageCoroutine);
    }


    // PRIVATE

    private AudioSource GetFreeSfxSource()
    {
        AudioSource freeSource = sfxList.Find( x => !x.isPlaying );
        if (freeSource == null)
            freeSource = CreateSfxSource();
        return freeSource;
    }


    private AudioSource CreateSfxSource()
    {
        var go = new GameObject("sfx");
        go.transform.SetParent(rootSfx);
        go.transform.ResetTransform();
        var asource = go.AddComponent<AudioSource>();
        asource.loop = false;
        asource.playOnAwake = false;
        asource.outputAudioMixerGroup = sfxMixerGroup;

        sfxList.Add(asource);
        return asource;
    }




    private void SetDamageSnapshot(bool isDamaged, float time)
    {
        var mixer = sfxMixerGroup.audioMixer;
        var main = mixer.FindSnapshot("snapshot-main");
        var damage = mixer.FindSnapshot("snapshot-damage");
        // this should be cached
        var weights = new[] { isDamaged? 0f:1f, isDamaged? 1f:0f};
        mixer.TransitionToSnapshots(new[] {main, damage}, weights, time);
    }

    private IEnumerator SnapshotTransitionCR(float time)
    {
        this.SetDamageSnapshot(true, time);
        yield return new WaitForSeconds(time);
        this.SetDamageSnapshot(false, time);
        this.damageCoroutine = null;
    }

}


