﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




public class UIShellClipSlot : MonoBehaviour
{

    private UIShellClip activeUI;

    public UIShellClip Active {
        get {
            Assert.IsNotNull(activeUI);
            return activeUI;
        }
    }


    public void CreateShellClip(UIShellClip prototype)
    {
        Assert.IsNotNull(prototype);

        if (activeUI != null)
            Destroy(activeUI.gameObject);

        activeUI = Instantiate(prototype, Vector3.zero, Quaternion.identity, this.transform);
        activeUI.transform.localPosition = Vector3.zero;
    }
}
