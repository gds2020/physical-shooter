﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIShellClip : MonoBehaviour
{
    [SerializeField] Transform rootXF;
    private int displayedCount;


    public int DisplayedCount {
        get => displayedCount;
        set {
            Debug.Assert(value >= 0);
            displayedCount = value;
            ShowShells(displayedCount);
        }
    }


    private void ShowShells(int count)
    {
        int childCount = rootXF.childCount;
        Debug.Assert(count <= childCount, $"bullet count:{count} | childCount:{childCount} | name: {name}");

        for (int i = 0; i < childCount; ++i)
            rootXF.GetChild(i).gameObject.SetActive(i < count);
    }
}
