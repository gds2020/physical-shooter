﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;




public class UIGameMenu : MonoBehaviour
{
    [SerializeField] Button buttonBack;
    [SerializeField] Button buttonExit;


    public bool IsVisible {
        get => this.gameObject.activeSelf;
        set => this.gameObject.SetActive(value);
    }


    void Awake()
    {
        buttonBack.onClick.AddListener( () => this.gameObject.SetActive(false) );
        buttonExit.onClick.AddListener( () => SceneManager.LoadScene(Const.SceneNames.Menu) );
    }

}
