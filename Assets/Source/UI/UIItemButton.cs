﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;



namespace InventorySystem
{
    public class UIItemButton : MonoBehaviour
    {
        [SerializeField] Toggle toggle;
        [SerializeField] Text textComponnent;
        [SerializeField] Color selectedColor;
        [SerializeField] Color unSelectedColor;


        private Item targetItem;


        public event Action<Item> OnSelected;


        public bool IsOn {
            get { return toggle.isOn;  }
            set { 
                toggle.isOn = value;
                OnToggleValueChanged(value);
            }
        }


        void OnEnable()
        {
            this.OnToggleValueChanged(toggle.isOn);
        }


        public void Setup(Item i)
        {
           textComponnent.text = i.Name;
           targetItem = i;
           toggle.onValueChanged.AddListener(OnToggleValueChanged);
        }


        public void OnToggleValueChanged(bool toggleOn)
        {
            textComponnent.color = toggleOn ? selectedColor : unSelectedColor;
            if (toggleOn)
                OnSelected?.Invoke(targetItem);
        }
    }
}