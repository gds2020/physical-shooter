﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;






namespace InventorySystem
{
    public class UIInventoryController : MonoBehaviour
    {
        [SerializeField] Toggle toggleWeapon;
        [SerializeField] Toggle toggleAmmo;
        [SerializeField] Toggle toggleConsumable;

        private Inventory inventory;
        private UIItemListController itemList;
        private UIItemInfoController itemInfo;


        public bool IsVisible => this.gameObject.activeSelf;


        void Awake()
        {
            toggleWeapon.onValueChanged.AddListener    ( IsOn => ShowCategory(EItemType.Weapon, IsOn) );
            toggleAmmo.onValueChanged.AddListener      ( IsOn => ShowCategory(EItemType.Ammo, IsOn) );
            toggleConsumable.onValueChanged.AddListener( IsOn => ShowCategory(EItemType.Consumable, IsOn) );
            // find
            itemList = GetComponentInChildren<UIItemListController>(true);
            itemInfo = GetComponentInChildren<UIItemInfoController>(true);
            // assert
            Assert.IsNotNull(itemList, "child not found");
            Assert.IsNotNull(itemInfo);
            // prepare objects
            itemInfo.gameObject.SetActive(false);
            itemList.gameObject.SetActive(true);
            // subscribe
            itemList.OnSomeButtonPressed += ItemButtonPressed;
        }



        public void ShowAndSetup(Inventory inv)
        {
            Assert.IsNotNull(inv);
            inventory = inv;
            // show object
            this.gameObject.SetActive(true);
        }


        public void Hide()
        {
            this.gameObject.SetActive(false);
            inventory = null;
        }




        private void ShowCategory(EItemType itemType, bool IsToggleOn)
        {
            Assert.IsNotNull(inventory);
            if (!IsToggleOn) return;

            itemInfo.gameObject.SetActive(false);
            Item[] filtered = System.Array.FindAll(inventory.AllItems, i => i.Type == itemType );
            itemList.Setup(filtered);
        }


        private void ItemButtonPressed(Item i)
        {
            Assert.IsNotNull(i);
            itemInfo.gameObject.SetActive(true);
            itemInfo.Setup(i);
        }
 
    }
}