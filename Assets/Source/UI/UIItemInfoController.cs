﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;



namespace InventorySystem
{

    public class UIItemInfoController : MonoBehaviour
    {
        [SerializeField] Image imageIcon;
        [SerializeField] Text  textName;
        [SerializeField] Text  textMass;
        [SerializeField] Text  textDescription;
        [SerializeField] Button actionButton;

        private Item currentItem;


        string nameInfoFormat;
        string massInfoFormat;


        private Text ActionButtonText => _at_ = _at_ ?? actionButton.GetComponentInChildren<Text>(true);
        Text _at_;


        private PlayerController Player {
            get {
                if (_pl_ == null) {
                    var go = GameObject.FindGameObjectWithTag("Player");
                    Assert.IsNotNull(go);
                    _pl_ = go.GetComponentAsserted<PlayerController>();
                }
                return _pl_;
            }
        } PlayerController _pl_;


        void Awake()
        {
            nameInfoFormat = textName.text;
            massInfoFormat = textMass.text;

            object o = null;
            object result = o ?? (object)1f;
        }


        public void Setup(Item item)
        {
            Assert.IsNotNull(item);
            currentItem = item;
            Assert.IsTrue(this.gameObject.activeInHierarchy, "info panel is hidden");
            textName.text = string.Format(nameInfoFormat, item.Name);
            textMass.text = string.Format(massInfoFormat, item.Mass);
            textDescription.text = item.Description;
            imageIcon.sprite = item.Icon;

            PrepareActionButton(item.Type);
        }


        private void PrepareActionButton(EItemType itemType)
        {

            switch (itemType)
            {
                case EItemType.Weapon:
                    actionButton.gameObject.SetActive(true);
                    ActionButtonText.text = "Equip";
                    actionButton.onClick.RemoveAllListeners();
                    actionButton.onClick.AddListener(EquipButtonPressed_Handler);
                    return;

                case EItemType.Ammo:
                    actionButton.gameObject.SetActive(false);
                    return;

                case EItemType.Consumable:
                    actionButton.gameObject.SetActive(true);
                    ActionButtonText.text = "Use";
                    actionButton.onClick.RemoveAllListeners();
                    actionButton.onClick.AddListener(UseButtonPressed_Handler);
                    return;

                default:
                    throw new System.NotImplementedException(itemType.ToString());
            }
        }


        private void EquipButtonPressed_Handler()
        {
            Assert.IsNotNull(currentItem);
            Player.EquipWeapon((WeaponItem)currentItem);
        }


        private void UseButtonPressed_Handler()
        {
            Assert.IsNotNull(currentItem);
            Player.UseConsumable(currentItem);
        }

    }
}