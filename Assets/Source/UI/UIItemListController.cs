using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;



namespace InventorySystem
{
    public class UIItemListController : MonoBehaviour
    {
        [SerializeField] UIItemButton buttonPrototype;
        List<UIItemButton> activeButtons = new List<UIItemButton>();


        public event Action<Item> OnSomeButtonPressed;


        public void Setup(Item[] items)
        {
            Assert.IsNotNull(items);

            this.Cleanup();
            for (int i = 0; i < items.Length; i++)
                CreateButton(items[i]);

            if (activeButtons.Count != 0)
                activeButtons[0].IsOn = true;
        }


        private void Cleanup()
        {
            for (int i = 0; i < activeButtons.Count; i++)
            {
                Destroy(activeButtons[i].gameObject);
            }
            activeButtons.Clear();
        }


        private void CreateButton(Item i)
        {
            Assert.IsNotNull(i);
            UIItemButton newButton = GameObject.Instantiate(buttonPrototype, Vector3.zero, Quaternion.identity, this.transform);
            newButton.transform.localPosition = Vector3.zero;
            newButton.name = $"ClonedItemButton<{i.Name}>";
            newButton.gameObject.SetActive(true);
            newButton.Setup(i);
            newButton.OnSelected += SomeButtonClicked;
            activeButtons.Add(newButton);
        }


        private void SomeButtonClicked(Item i)
        {
            //Debug.Log("pressed button with id: " + i.Name);
            Assert.IsNotNull(this.OnSomeButtonPressed, "noone listen for this even");
            this.OnSomeButtonPressed.Invoke(i);
        }
    }
}